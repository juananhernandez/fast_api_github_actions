from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.application import user

app = FastAPI()

app.include_router(user.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # FIXME To be modified when running in production.
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api")
async def root():
    return {"message": "Hello World"}
